# frozen_string_literal: true

module GitlabKramdown
  module Parser
    # GitLab Labels
    #
    # This parser implements non context-specific label references as described
    # in the GitLab Flavored Markdown reference
    #
    # @see https://docs.gitlab.com/ee/user/markdown.html#special-gitlab-references
    module Label
      PROJECT_LABEL_PATTERN = %r{
        #{Regexp.escape('~')}
        (?<label>
          [A-Za-z0-9_\-?.&]+ | # String-based single-word label title, or
          ".+?"                # String-based multi-word label surrounded in quotes
        )
      }x.freeze

      def self.included(klass)
        klass.define_parser(:label, PROJECT_LABEL_PATTERN, '\~')
      end

      def parse_label
        start_line_number = @src.current_line_number
        @src.pos += @src.matched_size

        namespace = extract_reference_namespace!

        # If we can't find a namespace we just add content as regular text and skip
        unless namespace
          add_text(@src[0])

          return
        end

        label_name = @src[:label].delete('"')
        label_param = label_name.tr(' ', '+')
        href = "#{@options[:gitlab_url]}/#{namespace}/issues?label_name=#{label_param}"
        label_attrs = { 'href' => href, 'label' => label_name, 'namespace' => namespace }

        el = Kramdown::Element.new(:label, nil, label_attrs, location: start_line_number)

        @tree.children << el
      end
    end
  end
end
