# frozen_string_literal: true

module GitlabKramdown
  VERSION = '0.21.0'
end
