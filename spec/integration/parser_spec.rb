# frozen_string_literal: true

require 'spec_helper'

context 'with GitLab Kramdown Integration tests' do
  let(:options) do
    { input: 'GitlabKramdown', linkable_headers: false, autolink: true }
  end

  context 'with HTML render' do
    shared_examples 'render elements to html' do |text_file, html_file|
      it "renders #{File.basename(text_file)} as #{File.basename(html_file)}" do
        source = File.read(text_file)
        target = File.read(html_file)
        parser = Kramdown::Document.new(source, options)

        expect(parser.to_html).to eq(target)
      end
    end

    Fixtures.text_files.each do |text_file|
      include_examples 'render elements to html', text_file, Fixtures.html_file(text_file)
    end

    context 'when linkable_headers enabled' do
      let(:options) do
        { input: 'GitlabKramdown', linkable_headers: true }
      end

      include_examples 'render elements to html',
                       File.join(Fixtures.fixtures_path, 'header.text'),
                       File.join(Fixtures.fixtures_path, 'header.linkable_headers.html')
    end

    context 'when clickable_images disabled' do
      let(:options) do
        { input: 'GitlabKramdown', clickable_images: false }
      end

      include_examples 'render elements to html',
                       File.join(Fixtures.fixtures_path, 'image.text'),
                       File.join(Fixtures.fixtures_path, 'image.clickable_images_disabled.html')

      include_examples 'render elements to html',
                       File.join(Fixtures.fixtures_path, 'plantuml.text'),
                       File.join(Fixtures.fixtures_path, 'plantuml.clickable_images_disabled.html')
    end

    context 'with references without autolink enabled' do
      let(:options) do
        { input: 'GitlabKramdown' }
      end

      include_examples 'render elements to html',
                       File.join(Fixtures.fixtures_path, 'references.text'),
                       File.join(Fixtures.fixtures_path, 'references.autolink_disabled.html')
    end
  end
end
